package com.eteo.teamc.function.task.converter;

import com.eteo.teamc.function.task.domain.Task;
import com.eteo.teamc.function.task.dto.TaskSummaryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskConverter {

    private TaskStatusConverter statusConverter;

    @Autowired
    public TaskConverter(TaskStatusConverter statusConverter) {
        this.statusConverter = statusConverter;
    }

    public TaskSummaryDto toTaskSummary(Task task) {
        TaskSummaryDto s = new TaskSummaryDto();
        s.id = task.getId();
        s.title = task.getTitle();
        s.description = task.getDescription();
        s.assigneeId = task.getAssignee().getId();
        s.creationTime = task.getCreationTime();
        s.creator = task.getCreator();
        s.status = statusConverter.toTaskStatusDto(task.getStatus());
        s.deadline = task.getDeadline();
        return s;
    }
}
