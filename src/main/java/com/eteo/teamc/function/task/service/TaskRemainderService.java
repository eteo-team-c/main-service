package com.eteo.teamc.function.task.service;

import com.eteo.teamc.function.task.dto.TaskSummaryDto;
import com.eteo.teamc.function.user.service.UserService;
import com.eteo.teamc.shared.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TaskRemainderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskRemainderService.class);

    private TaskService taskService;
    private UserService userService;
    private MailSender mailSender;

    public TaskRemainderService(TaskService taskService, UserService userService, MailSender mailSender) {
        this.taskService = taskService;
        this.userService = userService;
        this.mailSender = mailSender;
    }

    public void check() {
        LocalDateTime utcNow = OffsetDateTime.now()
            .withOffsetSameInstant(ZoneOffset.UTC)
            .toLocalDateTime();
        List<TaskSummaryDto> expiredTasks = taskService.getNonNotifiedExpiredTasks(utcNow);
        LOGGER.info("Found {} expired tasks", expiredTasks.size());
        if (expiredTasks.isEmpty()) {
            return;
        }

        for (TaskSummaryDto expiredTask : expiredTasks) {
            send(expiredTask);
        }
        taskService.markTasksAsNotified(expiredTasks.stream()
                .map(task -> task.id)
                .collect(toList()));
    }

    private void send(TaskSummaryDto taskSummary) {
        UserDTO assignee = userService.findById(taskSummary.assigneeId);

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom("kereszi.zsolt@gmail.com");
        mail.setTo(assignee.getEmail());
        mail.setSubject("Your task has expired");
        mail.setText("Title: " + taskSummary.title + "\nDescription: " + taskSummary.description);
        mailSender.send(mail);
    }
}
