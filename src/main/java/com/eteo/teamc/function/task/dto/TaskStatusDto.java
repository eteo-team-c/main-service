package com.eteo.teamc.function.task.dto;

public class TaskStatusDto {
    public long id;
    public String name;
    public boolean isProtected;
}
