package com.eteo.teamc.function.task.service;

import com.eteo.teamc.function.task.converter.TaskConverter;
import com.eteo.teamc.function.task.domain.Task;
import com.eteo.teamc.function.task.domain.TaskStatus;
import com.eteo.teamc.function.task.dto.CreateTaskRequest;
import com.eteo.teamc.function.task.dto.ModifyTaskRequest;
import com.eteo.teamc.function.task.dto.TaskSummaryDto;
import com.eteo.teamc.function.task.repo.TaskRepository;
import com.eteo.teamc.function.task.repo.TaskStatusRepository;
import com.eteo.teamc.function.user.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TaskService {

    private TaskRepository taskRepository;
    private TaskStatusRepository taskStatusRepository;
    private TaskConverter taskConverter;
    private UserRepository userRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository, TaskStatusRepository taskStatusRepository, TaskConverter taskConverter, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.taskStatusRepository = taskStatusRepository;
        this.taskConverter = taskConverter;
        this.userRepository = userRepository;
    }

    public List<TaskSummaryDto> list(Sort sort) {
        return taskRepository.findAll(sort).stream()
                .map(taskConverter::toTaskSummary)
                .collect(toList());
    }

    public void createTask(CreateTaskRequest createTaskRequest) {
        Task task = new Task();
        task.setTitle(createTaskRequest.title);
        task.setDescription(createTaskRequest.description);
        task.setAssignee(userRepository.getOne(createTaskRequest.assigneeId));
        task.setCreationTime(LocalDateTime.now());
        task.setDeadline(createTaskRequest.deadline);
        task.setStatus(taskStatusRepository.findByName(TaskStatus.STATUS_CREATED));
        task.setCreator("admin");
        taskRepository.save(task);
    }

    public void modifyTask(ModifyTaskRequest request) {
        Task task = taskRepository.findOne(request.id);
        task.setTitle(request.title);
        task.setDescription(request.description);
        task.setDeadline(request.deadline);
        task.setAssignee(userRepository.getOne(request.assigneeId));
        task.setStatus(taskStatusRepository.getOne(request.statusId));
        taskRepository.save(task);
    }

    public void delete(long id) {
        taskRepository.delete(id);
    }

    public void replaceStatuses(long id, long replacementId) {
        taskRepository.replaceStatuses(id, replacementId);
    }

    public List<TaskSummaryDto> getNonNotifiedExpiredTasks(LocalDateTime when) {
        return taskRepository.getNonNotifiedExpiredTasks(when).stream()
                .map(taskConverter::toTaskSummary)
                .collect(toList());
    }

    public void markTasksAsNotified(List<Long> taskIds) {
        taskRepository.markTasksAsNotified(taskIds);
    }
}
