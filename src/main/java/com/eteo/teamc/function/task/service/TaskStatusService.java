package com.eteo.teamc.function.task.service;

import com.eteo.teamc.function.task.converter.TaskStatusConverter;
import com.eteo.teamc.function.task.domain.TaskStatus;
import com.eteo.teamc.function.task.dto.TaskStatusDto;
import com.eteo.teamc.function.task.repo.TaskStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TaskStatusService {

    private TaskService taskService;
    private TaskStatusRepository taskStatusRepository;
    private TaskStatusConverter taskStatusConverter;

    @Autowired
    public TaskStatusService(TaskService taskService, TaskStatusRepository taskStatusRepository, TaskStatusConverter taskStatusConverter) {
        this.taskService = taskService;
        this.taskStatusRepository = taskStatusRepository;
        this.taskStatusConverter = taskStatusConverter;
    }

    public List<TaskStatusDto> list() {
        return taskStatusRepository.findAll().stream()
                .map(taskStatusConverter::toTaskStatusDto)
                .collect(toList());
    }

    public void createStatus(String name) {
        taskStatusRepository.save(new TaskStatus(name));
    }

    @Transactional
    public void delete(long id, long replacementId) {
        taskService.replaceStatuses(id, replacementId);
        taskStatusRepository.delete(id);
    }

    public void modify(long id, String newName) {
        TaskStatus taskStatus = taskStatusRepository.findOne(id);
        taskStatus.setName(newName);
        taskStatusRepository.save(taskStatus);
    }
}
