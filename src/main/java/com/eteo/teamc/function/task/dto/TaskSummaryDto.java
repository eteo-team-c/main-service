package com.eteo.teamc.function.task.dto;

import java.time.LocalDateTime;

public class TaskSummaryDto {

    public Long id;
    public String title;
    public String description;
    public String creator;
    public long assigneeId;
    public TaskStatusDto status;
    public LocalDateTime creationTime;
    public LocalDateTime deadline;

}
