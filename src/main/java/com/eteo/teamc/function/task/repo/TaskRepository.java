package com.eteo.teamc.function.task.repo;

import com.eteo.teamc.function.task.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Modifying
    @Query(value = "update task t set t.status_id = :replacementId where t.status_id = :id",
            nativeQuery = true)
    void replaceStatuses(@Param("id") long id, @Param("replacementId") long replacementId);

    @Modifying
    @Query(value = "update Task t set t.notified = true where t.id in (:ids)")
    void markTasksAsNotified(@Param("ids") List<Long> ids);

    @Query(value = "select t from Task t where " +
            "t.notified = false and t.deadline <= :now")
    List<Task> getNonNotifiedExpiredTasks(@Param("now") LocalDateTime now);


}
