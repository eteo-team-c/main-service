package com.eteo.teamc.function.task.controller;

import com.eteo.teamc.function.task.dto.CreateTaskRequest;
import com.eteo.teamc.function.task.dto.ModifyTaskRequest;
import com.eteo.teamc.function.task.dto.TaskSummaryDto;
import com.eteo.teamc.function.task.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/task")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create")
    public void createTask(@RequestBody CreateTaskRequest createTaskRequest) {
        taskService.createTask(createTaskRequest);
    }

    @PutMapping("/modify")
    public void modifyTask(@RequestBody ModifyTaskRequest modifyTaskRequest) {
        taskService.modifyTask(modifyTaskRequest);
    }

    @GetMapping("/list")
    public List<TaskSummaryDto> list(Sort sort) {
        return taskService.list(sort);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        taskService.delete(id);
    }
}
