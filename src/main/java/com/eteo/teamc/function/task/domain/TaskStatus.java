package com.eteo.teamc.function.task.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "task_status")
public class TaskStatus {

    public static final String STATUS_CREATED = "created";
    public static final String STATUS_DONE = "done";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "protected", nullable = false)
    private boolean isProtected;

    public TaskStatus() {
    }

    public TaskStatus(String name) {
        this.name = name;
        this.isProtected = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public void setProtected(boolean p) {
        this.isProtected = p;
    }
}
