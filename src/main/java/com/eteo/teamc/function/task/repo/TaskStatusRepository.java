package com.eteo.teamc.function.task.repo;

import com.eteo.teamc.function.task.domain.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TaskStatusRepository extends JpaRepository<TaskStatus, Long> {

    @Query("select t from TaskStatus t " +
            "where t.name = :name")
    TaskStatus findByName(@Param("name") String name);
}
