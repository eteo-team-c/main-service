package com.eteo.teamc.function.task.dto;

import java.time.OffsetDateTime;

public class ModifyTaskRequest {

    public long id;
    public long statusId;
    public String title;
    public String description;
    public long assigneeId;
    public OffsetDateTime deadline;
}
