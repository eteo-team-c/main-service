package com.eteo.teamc.function.task.converter;

import com.eteo.teamc.function.task.domain.TaskStatus;
import com.eteo.teamc.function.task.dto.TaskStatusDto;
import org.springframework.stereotype.Component;

@Component
public class TaskStatusConverter {

    public TaskStatusDto toTaskStatusDto(TaskStatus s) {
        TaskStatusDto tsd = new TaskStatusDto();
        tsd.id = s.getId();
        tsd.name = s.getName();
        tsd.isProtected = s.isProtected();
        return tsd;
    }
}
