package com.eteo.teamc.function.task.job;

import com.eteo.teamc.function.task.service.TaskRemainderService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class RemainderJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemainderJob.class);

    @Autowired
    private TaskRemainderService taskRemainderService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        taskRemainderService.check();
        LOGGER.info("remainder job started");
    }
}
