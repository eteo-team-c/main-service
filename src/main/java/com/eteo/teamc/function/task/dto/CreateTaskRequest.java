package com.eteo.teamc.function.task.dto;

import java.time.OffsetDateTime;

public class CreateTaskRequest {

    public String title;
    public String description;
    public long assigneeId;
    public OffsetDateTime deadline;
}
