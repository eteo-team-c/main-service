package com.eteo.teamc.function.task.controller;

import com.eteo.teamc.function.task.dto.TaskStatusDto;
import com.eteo.teamc.function.task.service.TaskStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/task-status")
public class TaskStatusController {

    private TaskStatusService taskStatusService;

    @Autowired
    public TaskStatusController(TaskStatusService taskStatusService) {
        this.taskStatusService = taskStatusService;
    }

    @PostMapping("/create")
    public void createTaskStatus(@RequestBody String name) {
        taskStatusService.createStatus(name);
    }

    @PutMapping("/modify/{id}")
    public void modifyTaskStatus(@PathVariable long id, @RequestBody String newName) {
        taskStatusService.modify(id, newName);
    }

    @GetMapping("/list")
    public List<TaskStatusDto> list() {
        return taskStatusService.list();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id, long replacementStatusId) {
        taskStatusService.delete(id, replacementStatusId);
    }
}
