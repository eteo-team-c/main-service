package com.eteo.teamc.function.user.service;

import com.eteo.teamc.function.user.domain.User;
import com.eteo.teamc.function.user.repo.UserRepository;
import com.eteo.teamc.shared.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User registerUser(UserDTO userDTO) {

        User newUser = new User();
        newUser.setId(userDTO.getId());
        newUser.setLogin(userDTO.getLogin());
        // new user gets initially a generated password
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                user.setLogin(userDTO.getLogin());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());
                user.setImageUrl(userDTO.getImageUrl());
                user.setLangKey(userDTO.getLangKey());
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserService::toUserDto);
    }

    public void deleteUser(String login) {
        userRepository.deleteByLogin(login);
    }

    public UserDTO findById(long id) {
        return toUserDto(userRepository.findOne(id));
    }

    public List<UserDTO> getAll() {
        return userRepository.findAll().stream()
            .map(UserService::toUserDto)
            .collect(toList());
    }

    private static UserDTO toUserDto(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        // new user gets initially a generated password
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setImageUrl(user.getImageUrl());
        userDTO.setLangKey(user.getLangKey());
        return userDTO;
    }
}
