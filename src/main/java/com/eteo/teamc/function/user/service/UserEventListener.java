package com.eteo.teamc.function.user.service;

import com.eteo.teamc.shared.UserDTO;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

@MessageEndpoint
public class UserEventListener {

    private UserService userService;

    public UserEventListener(UserService userService) {
        this.userService = userService;
    }

    @ServiceActivator(inputChannel = "distributedQueueChannel", poller = @Poller(fixedRate = "10"))
    public void handleUserMessage(Message<?> message) {
        Message<?> payload = (Message<?>) message.getPayload();
        String type = String.valueOf(payload.getHeaders().get("TYPE"));
        handle(type, payload.getPayload());
    }

    private void handle(String type, Object payload) {
        switch (type) {
            case "created":
                userService.registerUser((UserDTO) payload);
                break;
            case "modified":
                userService.updateUser((UserDTO) payload);
                break;
            case "deleted":
                userService.deleteUser(String.valueOf(payload));
                break;
        }
    }
}
