package com.eteo.teamc.function.user.controller;

import com.eteo.teamc.function.user.service.UserService;
import com.eteo.teamc.shared.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/task-user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping()
    public void createUser(@RequestBody UserDTO userDTO) {
        userService.registerUser(userDTO);
    }

    @GetMapping("/list")
    public List<UserDTO> list() {
        return userService.getAll();
    }
}
