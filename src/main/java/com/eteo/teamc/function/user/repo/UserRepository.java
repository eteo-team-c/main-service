package com.eteo.teamc.function.user.repo;

import com.eteo.teamc.function.user.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Modifying
    @Query("delete from User u where u.login = :login")
    void deleteByLogin(@Param("login") String login);
}
