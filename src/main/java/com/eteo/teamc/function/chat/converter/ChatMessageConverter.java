package com.eteo.teamc.function.chat.converter;

import com.eteo.teamc.function.chat.domain.ChatMessage;
import com.eteo.teamc.function.chat.domain.dto.ChatMessageDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ChatMessageConverter {

    public static ChatMessageDto convert(ChatMessage chatMessage) {
        ChatMessageDto chatMessageDto = new ChatMessageDto();
        chatMessageDto.content = chatMessage.getContent();
        chatMessageDto.sender = chatMessage.getSender();
        chatMessageDto.sendTime = chatMessage.getSendTime();
        return chatMessageDto;
    }

    public static ChatMessage convert(ChatMessageDto chatMessageDto) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setContent(chatMessageDto.content);
        chatMessage.setSender(chatMessageDto.sender);
        chatMessage.setSendTime(chatMessageDto.sendTime);
        return chatMessage;
    }

    public static List<ChatMessageDto> convertAllEntities(Collection<ChatMessage> chatMessages) {
        return chatMessages.stream().map(ChatMessageConverter::convert).collect(Collectors.toList());
    }

    public static List<ChatMessage> convertAllDtos(Collection<ChatMessageDto> chatMessages) {
        return chatMessages.stream().map(ChatMessageConverter::convert).collect(Collectors.toList());
    }
}
