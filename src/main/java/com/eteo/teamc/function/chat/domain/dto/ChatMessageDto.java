package com.eteo.teamc.function.chat.domain.dto;

import java.util.Date;

public class ChatMessageDto {

    public String content;
    public String sender;
    public Date sendTime;
}
