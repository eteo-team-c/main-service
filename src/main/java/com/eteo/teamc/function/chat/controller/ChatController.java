package com.eteo.teamc.function.chat.controller;

import com.eteo.teamc.function.chat.domain.dto.ChatMessageDto;
import com.eteo.teamc.function.chat.service.ChatService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/chat")
public class ChatController {

    private ChatService service;

    public ChatController(ChatService service) {
        this.service = service;
    }

    @PostMapping("/submit-message")
    public void submitMessage(@RequestBody ChatMessageDto chatMessage) {
        service.submitMessage(chatMessage);
    }

    @GetMapping("/existing-messages")
    public List<ChatMessageDto> getExistingMessages() {
        return service.getExistingChatMessages();
    }
}
