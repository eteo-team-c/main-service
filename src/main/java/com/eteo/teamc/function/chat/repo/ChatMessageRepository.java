package com.eteo.teamc.function.chat.repo;

import com.eteo.teamc.function.chat.domain.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, String> {
}
