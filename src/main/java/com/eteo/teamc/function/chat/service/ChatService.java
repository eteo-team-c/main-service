package com.eteo.teamc.function.chat.service;

import com.eteo.teamc.function.chat.converter.ChatMessageConverter;
import com.eteo.teamc.function.chat.domain.ChatMessage;
import com.eteo.teamc.function.chat.domain.dto.ChatMessageDto;
import com.eteo.teamc.function.chat.repo.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChatService {

    private ChatMessageRepository chatMessageRepository;

    @Autowired
    public ChatService(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    public void submitMessage(ChatMessageDto chatMessageDto){
        ChatMessage chatMessage = ChatMessageConverter.convert(chatMessageDto);
        chatMessageRepository.save(chatMessage);
    }

    public List<ChatMessageDto> getExistingChatMessages(){
        List<ChatMessage> chatMessages = chatMessageRepository.findAll();
        return ChatMessageConverter.convertAllEntities(chatMessages);
    }
}
