package com.eteo.teamc.config;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.hazelcast.CacheListeningPolicyType;
import org.springframework.integration.hazelcast.inbound.HazelcastEventDrivenMessageProducer;
import org.springframework.messaging.PollableChannel;

@Configuration
@ImportResource("classpath:config/hazelcast-spring.xml")
@EnableIntegration
public class HazelcastConfig {

    @Bean
    public PollableChannel distributedQueueChannel() {
        return new QueueChannel();
    }

    @Bean
    public IQueue<?> distributedQueue(HazelcastInstance hazelcastInstance) {
        return hazelcastInstance.getQueue("uaa-user");
    }

    @Bean
    public HazelcastEventDrivenMessageProducer hazelcastEventDrivenMessageProducer(IQueue<?> distributedQueue, PollableChannel distributedQueueChannel) {
        final HazelcastEventDrivenMessageProducer producer = new HazelcastEventDrivenMessageProducer(distributedQueue);
        producer.setOutputChannel(distributedQueueChannel);
        producer.setCacheListeningPolicy(CacheListeningPolicyType.ALL);
        return producer;
    }
}
