package com.eteo.teamc.config;

import com.eteo.teamc.util.AutowiringSpringBeanJobFactory;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;

@Configuration
public class QuartzConfig {

    @Bean
    public JobFactory springBeanJobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean scheduler(DataSource dataSource, JobFactory jobFactory, JobDetail jobDetail, Trigger trigger) {
        SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));

        schedulerFactory.setDataSource(dataSource);
        schedulerFactory.setExposeSchedulerInRepository(true);
        schedulerFactory.setJobFactory(jobFactory);
        schedulerFactory.setJobDetails(jobDetail);
        schedulerFactory.setTriggers(trigger);
        return schedulerFactory;
    }

//    @Bean
//    public SchedulerFactoryBeanCustomizer schedulerFactoryBeanCustomizer(JobFactory jobFactory) {
//        return schedulerFactoryBean -> {
//            schedulerFactoryBean.setExposeSchedulerInRepository(true);
//            schedulerFactoryBean.setConfigLocation(new ClassPathResource("quartz.properties"));
//            schedulerFactoryBean.setJobFactory(jobFactory);
//        };
//    }
}
