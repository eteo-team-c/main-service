/**
 * View Models used by Spring MVC REST controllers.
 */
package com.eteo.teamc.web.rest.vm;
