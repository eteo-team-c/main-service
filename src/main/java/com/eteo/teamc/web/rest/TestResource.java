package com.eteo.teamc.web.rest;

    import com.eteo.teamc.security.AuthoritiesConstants;
    import com.eteo.teamc.web.api.TestApi;
    import org.springframework.http.ResponseEntity;
    import org.springframework.security.access.annotation.Secured;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TestResource implements TestApi {

    @Override
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<String> testSecurity() {
        return ResponseEntity.ok("Minden fasza..");
    }
}
